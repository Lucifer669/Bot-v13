const { Command } = require('discord-akairo');
const { MessageEmbed } = require('discord.js');

class YoutubeCommand extends Command {
  constructor() {
    super("yt", {
      aliases: ["yt", "youtube"],
      category: 'Misc',
    });
  }

  async exec(message, args, level) {
    let youtube = args.slice(0).join('+');

    let link = `https://www.youtube.com/results?search_query=` + youtube;
    if (!youtube) return message.reply(`S'il vous plait entrer un mot !`);
    if (!link)
      return message.reply('Une erreur est survenue sur la commande youtube');

    const embed = new MessageEmbed()
      .setAuthor(this.client.user.username)
      .setColor('RANDOM')
      .setThumbnail(
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTGFxD8g-zZx4sJQvzYvYP21kPtFroeD2y6erwT9tOmI9N5kFn_'
      )
      .setTimestamp()
      .addField('Action:', 'Recherche sur youtube')
      .addField('Mot:', `${args.slice(0).join(' ')}`)
      .addField('Lien:', `${link}`)
      .setAuthor(this.client.user.username)
      .setFooter(`Recherche Youtube demander par ${message.author.username}`);
    message.channel.send({embeds: [embed]});
    message.author.send(`Vous avez rechercher ${link} dans ${message.guild.name}`);
  };
};
module.exports = YoutubeCommand;
