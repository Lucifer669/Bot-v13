const { Command } = require("discord-akairo");

class HelpCommand extends Command {
	constructor() {
		super("help", {
			aliases: ["help"],
            category: 'Misc',
			args: [{ id: "command", type: "commandAlias" }],
		});
	}
	exec(message, args) {
        const prefix = this.handler.prefix;
		const command = args.command;
		if (!command) {
			let embed = this.client.functions.embed()
				.setAuthor(
                    `Bonjour, mon nom est ${this.client.user.username}.`, 
                    this.client.user.displayAvatarURL()
                    )
				.setDescription(`Retrouvez la liste de toutes nos commandes ci-dessous.
                Si vous avez besoin d'assistance, rejoignez [notre server](https://placeholder.com)
                **----------**`);

                for (const category of this.handler.categories.values()) {
                    embed.addField(
                        `- ${category.id}`,
                        `${category
                            .filter(cmd => cmd.aliases.length > 0)
                        .map(cmd => `\`${cmd.aliases[0]}\``)
                        .join(' | ')}`
                  )
                }
                embed.addField(
                    '---------',
                    `**\`${prefix}help <command>\` pour des infos sur une commande spécifique.**
                    Exemples: \`${prefix}help ping\` | \`${prefix} help userinfo\``
                )

			return message.channel.send({ embeds: [embed] });
		}
	}
}
module.exports = HelpCommand;
