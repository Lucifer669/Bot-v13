const { Command } = require('discord-akairo');

class InfoCommand extends Command {
    constructor() {
        super('info', {
            aliases: ['info'],
            category: 'Misc',
            ownerOnly: false,
            channel: 'guild',
            args: [
                { id: 'member', type: 'member', default: message => message.member },
            ]
        });
    }
    exec(message, args) {

        return message.channel.send({ embeds: [
                this.client.functions.embed()
               .setTitle(`${args.member.displayName} (${args.member.id})`)
               .setThumbnail(args.member.user.displayAvatarURL())
               .setDescription(`Sont compte à été créer le ${args.member.user.createdAt}`)
         ]})
    }
}
module.exports = InfoCommand;