const { AkairoClient, Listener } = require("discord-akairo");

class ReadyListener extends Listener {
  constructor() {
    super("ready", {
      emitter: "client",
      event: "ready",
    });
  }

  exec() {
    this.client.user.setActivity(`Maintenance du bot, ${this.client.users.size} users | ${this.client.guilds.size} servers`)
    console.log("I'm ready!");
  }
}

module.exports = ReadyListener;
